import { Component, OnInit } from '@angular/core';
import { QuizService } from '../shared/quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  options: any[];
  cantidadPreguntas: number;

  constructor(private _quiz:QuizService, private route: Router){ 
    this._quiz.seconds = 0;
    this._quiz.question = 1;
    this._quiz.qnProgress = 0;


    this._quiz.obtenerPreguntas().subscribe((data:any)=>{
      this._quiz.qns = data.json();
      
      this.options = this.option(this._quiz.qnProgress);

      let quizLength = this._quiz.qns.length;
      this.cantidadPreguntas = 100 / quizLength;

      console.log(this._quiz.qns);
      this.startTime();
    })

  }

  ngOnInit() {
    
  }

  startTime(){
  	this._quiz.timer = setInterval(()=>{
  		this._quiz.seconds++;
  	},1000);
  }

  answer(qnid, respuesta){
    this._quiz.qns[this._quiz.qnProgress].answer = respuesta + 1;

    this._quiz.qnProgress++;

    if(this._quiz.qnProgress == this._quiz.qns.length){
      clearInterval(this._quiz.timer);
      this.route.navigate(['/result']);
    }
    this.options = this.option(this._quiz.qnProgress);
    this._quiz.question++;
  }

  option(progreso: number){  
      let options = this._quiz.qns[progreso].options;

      return this._quiz.qns[this._quiz.qnProgress].options = Object.keys(options).map(function(index){
          let option = options[index];
          return option;
      });

  }

}
