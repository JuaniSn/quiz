import { Component, OnInit } from '@angular/core';
import { QuizService } from '../shared/quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";


  constructor(private _quiz: QuizService, private route: Router) { }

  ngOnInit() {
  }

  registrarseQuiz(name: string, email: string){

  	this._quiz.insertarRegistro(name, email).subscribe((data: any) => {
          let respuesta = data.json();

          console.log(respuesta.name);
          localStorage.clear();
          localStorage.setItem('participante', JSON.stringify(respuesta));
          
          this.route.navigate(['/quiz']);
      });

  }
}
