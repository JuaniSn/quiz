import { Component, OnInit } from '@angular/core';
import { QuizService } from '../shared/quiz.service';
import { Router } from '@angular/router';

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private _quiz:QuizService, private route: Router){ }

  ngOnInit() {
  }

  salir(){
  	localStorage.clear();
  	clearInterval(this._quiz.timer);
  	this.route.navigate(['/register']);
  }

}
