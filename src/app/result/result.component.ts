import { Component, OnInit } from '@angular/core';
import { QuizService } from '../shared/quiz.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {
   
  participante: string;

  constructor(private _quiz: QuizService, private route: Router){

    this.participante = _quiz.getParticipante();

  }

  ngOnInit() {
  	this._quiz.obtenerRespuestas().subscribe((data) => {
  		let respuestas = data.json();
  		this._quiz.answerCorrectCount = 0;

  		this._quiz.qns.forEach((e,i)=>{

  			Object.defineProperty(e, 'correct', { writable: true });

  			if(e.answer == parseInt(respuestas[i].respuesta)){

  				this._quiz.answerCorrectCount++;

  				e.correct = respuestas[i].respuesta;
  			}
  			else{
  				e.correct = respuestas[i].respuesta;
  			}
  		
  		})

  		console.log(this._quiz.qns);
  	})
  }

  onSubmit(){
    this._quiz.submitScore();
  }

}
