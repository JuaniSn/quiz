import { Injectable } from '@angular/core';
import { Http } from '@angular/http';


@Injectable({
  providedIn: 'root'
})
export class QuizService {


  readonly url = 'http://localhost/excel/index.php/Quiz/';
  qns: any[];
  seconds: number;
  timer;
  qnProgress: number;
  question: number;
  answerCorrectCount;

  constructor(private http: Http) { }

  displayTimeElapsed(){
    return Math.floor(this.seconds/3600) + ':' + Math.floor(this.seconds/60) + ':' + Math.floor(this.seconds % 60);
  }


  insertarRegistro(name: string, email:string){
  	let data = {
  		name: name,
  		email: email
  	}
  	
    return this.http.post(this.url+'extraerDatosParticipante', JSON.stringify(data));
  
  }

  obtenerPreguntas(){
    return this.http.get(this.url+'traerPreguntas');
  }

  
  obtenerRespuestas(){
    let data = this.qns.map( x => x.qnid);
    return this.http.post(this.url+'respuestas', JSON.stringify(data));
  }

  getParticipante(){
    return JSON.parse(localStorage.getItem('participante'));
  }

  submitScore(){
    console.log(localStorage.getItem('participante'));
    var data = JSON.parse(localStorage.getItem('participante'));
    console.log(data);
    console.log(data.name);
    data.score = this.answerCorrectCount;
    data.tiempo = this.seconds;

    // return this.http.post()
  }
}
